import os
import sys
import pprint
import datetime


class CommonCore(object):
    DEBUG = False

    @staticmethod
    def year():
        return datetime.date.today().year

    @staticmethod
    def month():
        return datetime.date.today().month

    @staticmethod
    def day():
        return datetime.date.today().day

    @staticmethod
    def debug(s):
        if CommonCore.DEBUG:
            print("DEBUG: %s" % s)

    @staticmethod
    def error_exit(s, exit_code=1):
        print("ERROR: %s" % s)
        sys.exit(exit_code)

    @staticmethod
    def pretty(d):
        return pprint.pformat(d)
