from elasticsearch import Elasticsearch
from elasticsearch.exceptions import TransportError
from CommonCore import CommonCore


class ElasticsearchHelpers(object):

    def __init__(self, db_url=None, doc_type=None, timeout=30):
        if db_url == None:
            CommonCore.error_exit("Must specify the DB URL.")
        CommonCore.debug("db_url: %s" % db_url)
        if doc_type:
            self.doc_type = doc_type
        self._db = Elasticsearch([db_url], timeout=timeout)

    def db(self):
        return self._db

    def db_info(self):
        return self.db().info()

    def get_indices(self, index='*'):
        return self.db().indices.get(index)

    def get_latest_index(self, name):
        return ('%s-%s.%02d.%02d' % (name, CommonCore.year(),
                                           CommonCore.month(),
                                           CommonCore.day()))

    def insert_doc(self, index, doc_id, doc, doc_type=None):
        if doc_type == None:
            doc_type = self.doc_type
        else:
            CommonCore.error_exit("Must specify the doc_type.")

        CommonCore.debug("insert_doc ==> index: %s, doc_type: %s, id: %s, body: %s"
            % (index, doc_type, doc_id, doc))
        res = self.db().index(index=index, doc_type=doc_type, id=doc_id, body=doc)
        return res

    def update_doc(self, index, doc_id, doc, doc_type=None):
        if doc_type == None:
            doc_type = self.doc_type
        else:
            CommonCore.error_exit("Must specify the doc_type.")

        CommonCore.debug("update_doc ==> index: %s, doc_type: %s, id: %s, body: %s"
            % (index, doc_type, doc_id, doc))
        res = self.db().index(index=index, doc_type=doc_type, id=doc_id, body=doc)
        return res

    def delete_index(self, index):
        try:
            CommonCore.debug("delete_index ==> index: %s" % index)
            res = self.db().indices.delete(index=index)
            return res
        except TransportError, e:
            print("Exception code is '%s'." % e[0])
            if e[0] in ['N/A', 'TIMEOUT']:
                return None
            if int(e[0]) in [400, 404]:
                return None
            raise e

    def search(self, body, index, hits=30):
        CommonCore.debug("search ==> index: '%s', body: %s" % (index, body))
        res = self.db().search(index=index, params={'size':hits}, body=body)
        return res


