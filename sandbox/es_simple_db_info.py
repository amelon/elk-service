#!/usr/bin/env python
import os
import sys
import argparse

module_path = os.path.dirname(__file__) + '/../lib'
sys.path.insert(0, module_path)

from CommonCore import CommonCore
from ElasticsearchHelpers import ElasticsearchHelpers


def main():
    CommonCore.DEBUG = True

    if 'ELASTICSEARCH_URL' not in os.environ:
        CommonCore.error_exit("Environment variable ELASTICSEARCH_URL is not defined.")

    es = ElasticsearchHelpers(os.environ['ELASTICSEARCH_URL'], doc_type='simpledoc')
    print("Elasticsearch server info: %s" % CommonCore.pretty(es.db_info()))


if __name__ == '__main__':
    main()

