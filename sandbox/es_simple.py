#!/usr/bin/env python
import os
import sys
import argparse

module_path = os.path.dirname(__file__) + '/../lib'
sys.path.insert(0, module_path)

from CommonCore import CommonCore
from ElasticsearchHelpers import ElasticsearchHelpers


def prog_args():
    descr = """\
Populate DB with sample documents.
"""
    parser = argparse.ArgumentParser(prog=os.path.basename(__file__),
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=descr)
    # parser.add_argument('release', nargs=1, type=str,
    #                    help=("release name (e.g., BMF-7.2.0)"))
    # parser.add_argument('--max-results', metavar=('N'), default=0,
    #                    help=("Max issues to return. Default is 0 (no limit)."))
    _args = parser.parse_args()
    return _args


def main(args):
    CommonCore.DEBUG = True

    if 'ELASTICSEARCH_URL' not in os.environ:
        CommonCore.error_exit("Environment variable ELASTICSEARCH_URL is not defined.")

    es = ElasticsearchHelpers(os.environ['ELASTICSEARCH_URL'], doc_type='simpledoc')

    index = es.get_latest_index('simple')
    doc_id = 2
    doc = {
        "user": "kimchy",
        "post_date": "2009-11-15T14:12:12",
        "message": "trying out Elasticsearch"
    }

    # result = es.insert_doc(index=index, doc_id=doc_id, doc=doc)
    # result = es.get_indices()
    # print("result:\n%s" % helpers.prettify(result))

    # es.update_doc(index, 1, doc={"message": "revised"})
    # es.delete_index('twitter')
    # es.delete_index('twitter-2019.05.28')


if __name__ == '__main__':
    main(prog_args())

