#!/bin/sh -x

for index in logstash-2015.05.18 logstash-2015.05.19 logstash-2015.05.20; do
  curl -XPUT $ELASTICSEARCH_URL/$index -d '
    {
      "mappings": {
        "log": {
          "properties": {
            "geo": {
              "properties": {
                "coordinates": {
                  "type": "geo_point"
                }
              }
            }
          }
        }
      }
    }
    '
done


curl -XPUT $ELASTICSEARCH_URL/shakespeare -d '
{
 "mappings" : {
  "_default_" : {
   "properties" : {
    "speaker" : {"type": "string", "index" : "not_analyzed" },
    "play_name" : {"type": "string", "index" : "not_analyzed" },
    "line_id" : { "type" : "integer" },
    "speech_number" : { "type" : "integer" }
   }
  }
 }
}
'


