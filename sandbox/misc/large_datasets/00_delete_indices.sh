#!/bin/sh

for index in bank/ shakespeare/ logstash-2015.05.20/ logstash-2015.05.19/ logstash-2015.05.18/; do
    echo "Delete index $index"
    curl -XDELETE $ELASTICSEARCH_URL/$index
    echo ""
done

