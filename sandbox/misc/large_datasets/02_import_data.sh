#!/bin/sh -x

curl -XPOST $ELASTICSEARCH_URL/bank/account/_bulk?pretty -H 'Content-Type: application/json' --data-binary @accounts.json
curl -XPOST $ELASTICSEARCH_URL/shakespeare/_bulk?pretty -H 'Content-Type: application/json' --data-binary @shakespeare.json
curl -XPOST $ELASTICSEARCH_URL/_bulk?pretty -H 'Content-Type: application/json' --data-binary @logs.jsonl

