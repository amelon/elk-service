#!/usr/bin/env python

import os
import pprint
from elasticsearch import Elasticsearch

client = Elasticsearch([os.environ['ELASTICSEARCH_URL']])

res = client.search(
    index='keywords-2016.04.28',
    params={'size':10},
    body={
      'query': {
        'bool': {
          'must': [
            {'match_phrase': {'library':'Helpers'}},
            {'match_phrase': {'keyword':'copy'}}
          ]
        }
      }
    }
  )

print("result:\n%s" % pprint.pformat(res))

