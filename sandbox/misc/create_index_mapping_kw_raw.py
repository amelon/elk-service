#!/usr/bin/env python
# Note: This searches the not_analyzed (raw) field, so ES will attempt to do an
#       exact match (case sensitive). If you want case insensitive match, use
#       the analyzed field.

import os
import pprint
from elasticsearch import Elasticsearch

client = Elasticsearch([os.environ['ELASTICSEARCH_URL']])

res = client.indices.create(
    index='keywords-2016.xx.xx',
    body={
        "mappings": {
          "br_kw": {
            "properties": {
              "documentation": {
                "type": "string"
              },
              "keyword": {
                "type": "string",
                "fields": {
                  "raw": {
                    "type": "string",
                    "index": "not_analyzed"
                  }
                }
              },
              "library": {
                "type": "string"
              },
              "timestamp": {
                "type": "date",
                "format": "strict_date_optional_time||epoch_millis"
              }
            }
          }
        }
    }
  )

print("result:\n%s" % pprint.pformat(res))

