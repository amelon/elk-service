#!/bin/sh

index=vehicles
type=tv

id=one
curl -XPUT $ELASTICSEARCH_URL/$index/$type/$id \
     -H 'Content-Type: application/json' \
     -d'
  {
    "color": "green",
    "driver": {
      "born": "1959-09-07",
      "name": "Walter White"
    },
    "make": "Pontiac",
    "model": "Aztek",
    "value_usd": 5000.0,
    "year": 2003
  }'

