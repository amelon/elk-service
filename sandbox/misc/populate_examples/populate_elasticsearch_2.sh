#!/bin/sh

index=vehicles
type=tv

id=two
curl -XPUT $ELASTICSEARCH_URL/$index/$type/$id \
     -H 'Content-Type: application/json' \
     -d'
  {
    "color": "red",
    "driver": {
      "born": "1972-10-02",
      "name": "Jessica Rabbit"
    },
    "make": "Toyota",
    "model": "Camry",
    "value_usd": 15000.0,
    "year": 2014
  }'

