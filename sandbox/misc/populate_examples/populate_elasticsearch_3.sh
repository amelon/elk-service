#!/bin/sh

index=vehicles
type=tv

id=three
curl -XPUT $ELASTICSEARCH_URL/$index/$type/$id \
     -H 'Content-Type: application/json' \
     -d'
  {
    "color": "yellow",
    "driver": {
      "born": "1999-03-21",
      "name": "Paul McCartney"
    },
    "make": "Volkswagon",
    "model": "Beetle",
    "value_usd": 20000.0,
    "year": 2010
  }'

