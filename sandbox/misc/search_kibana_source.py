#!/usr/bin/env python
# This is taken out of a saved Kibana search (kibanaSavedObjectMeta.searchSourceJSON).
# It queries for all the docs in the library Helpers.

import os
import pprint
from elasticsearch import Elasticsearch

client = Elasticsearch([os.environ['ELASTICSEARCH_URL']])

res = client.search(
    index='keywords-2016.04.28',
    params={'size':10},
    body={
      'query': {
        "query_string": {
          "query": "_index:keywords-2016.04.28 AND library:Helpers",
          "analyze_wildcard": True
        }
      }
    }
  )

print("result:\n%s" % pprint.pformat(res))

