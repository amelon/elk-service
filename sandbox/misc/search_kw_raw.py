#!/usr/bin/env python
# Note: This searches the not_analyzed (raw) field, so ES will attempt to do an
#       exact match (case sensitive). If you want case insensitive match, use
#       the analyzed field.

import os
import pprint
from elasticsearch import Elasticsearch

client = Elasticsearch([os.environ['ELASTICSEARCH_URL']])

res = client.search(
    index='keywords-2016.04.28',
    params={'size':10},
    body={
      'query': {
        'bool': {
          'must': [
            {'match_phrase': {'library':'Helpers'}},
            {'match_phrase': {'keyword.raw':'Copy'}}
          ]
        }
      }
    }
  )

print("result:\n%s" % pprint.pformat(res))

