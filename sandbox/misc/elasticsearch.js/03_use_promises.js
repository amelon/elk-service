#!/usr/bin/env node
// Skip the callback to get a promise back

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

client.search({
  q: 'elasticsearch'
}).then(function(body) {
  var hits = body.hits.hits;
  //console.log(hits);

  hits.forEach(function(hit) {
    console.log("message: " + hit['_source']['message']);
  });

}, function(error) {
  console.trace(error.message);
});

