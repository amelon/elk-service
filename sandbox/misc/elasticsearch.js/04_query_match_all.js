#!/usr/bin/env node

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

client.search({
  //index: 'lab-inventory-2016.05.15',
  index: 'lab-reservation-2016.05.15',
  type: 'device',
  body: {
    query: {
      match_all: { }
    }
  }
}).then(function(body) {
  var hits = body.hits.hits;

  //console.log(hits);

  hits.forEach(function(hit) {
    console.log("device: " + hit['_source']['name'] + " (testbed: " + hit['_source']['testbed'] + ")");
  });

}, function(error) {
  console.trace(error.message);
});

