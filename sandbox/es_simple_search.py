#!/usr/bin/env python
import os
import sys
import argparse

module_path = os.path.dirname(__file__) + '/../lib'
sys.path.insert(0, module_path)

from CommonCore import CommonCore
from ElasticsearchHelpers import ElasticsearchHelpers


def prog_args():
    descr = """\
Search string in DB's message field.

Example:
%% %s --index simple-2019.05.28 --message elasticsearch --max-result=50
""" % (sys.argv[0])
    parser = argparse.ArgumentParser(prog=os.path.basename(__file__),
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=descr)
    parser.add_argument('--index', type=str, required=True,
                        help=("Index, i.e., database name (e.g., 'simple-2019.05.28')"))
    parser.add_argument('--message', type=str, required=True,
                        help=("Search string (e.g., 'elasticsearch')"))
    parser.add_argument('--max-results', metavar=('N'), type=int, default=10,
                        help=("Max results to return."))
    _args = parser.parse_args()
    return _args


def main(args):
    CommonCore.DEBUG = True

    if 'ELASTICSEARCH_URL' not in os.environ:
        CommonCore.error_exit("Environment variable ELASTICSEARCH_URL is not defined.")

    es = ElasticsearchHelpers(os.environ['ELASTICSEARCH_URL'], doc_type='simpledoc')

    body = {"query": {"match_phrase": {"message": args.message}}}
    result = es.search(body=body, index=args.index)
    print("Search result:\n%s" % CommonCore.pretty(result))
    hits = len(result['hits']['hits']) 
    if hits > 0:
        print("Search succeeded - hits: %d." % hits)
        sys.exit(0)
    else:
        print("Search failed - hits: %d." % hits)
        sys.exit(1)


if __name__ == '__main__':
    main(prog_args())

