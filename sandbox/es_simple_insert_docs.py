#!/usr/bin/env python
import os
import sys
import argparse

module_path = os.path.dirname(__file__) + '/../lib'
sys.path.insert(0, module_path)

from CommonCore import CommonCore
from ElasticsearchHelpers import ElasticsearchHelpers


def main():
    CommonCore.DEBUG = True

    if 'ELASTICSEARCH_URL' not in os.environ:
        CommonCore.error_exit("Environment variable ELASTICSEARCH_URL is not defined.")

    es = ElasticsearchHelpers(os.environ['ELASTICSEARCH_URL'], doc_type='simpledoc')

    index = es.get_latest_index('simple')

    docs = [
        {
            "user": "kimchy",
            "post_date": "2009-11-15T14:12:12",
            "message": "trying out Elasticsearch"
        },
        {
            "user": "nsap",
            "post_date": "2009-11-15T14:13:05",
            "message": "evaluating Elasticsearch and MongoDB"
        },
    ]

    doc_id = 0
    for doc in docs:
        doc_id += 1
        result = es.insert_doc(index=index, doc_id=doc_id, doc=doc)
        print("insert result:\n%s" % CommonCore.pretty(result))
        print('')


if __name__ == '__main__':
    main()

