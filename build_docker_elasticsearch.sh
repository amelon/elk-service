#!/bin/sh

id=8
image=amelon1/elasticsearch

docker build \
    -t $image:$id \
    -t $image:latest \
    -f Dockerfile-elasticsearch \
    .
