* Elasticsearch URL:

        % export ELASTICSEARCH_URL=http://localhost:9200

* Build the image:

        % build_docker_elasticsearch.sh
        % build_docker_dejavu.sh
        % build_docker_kibana.sh

* Run it:

        % docker-compose up -d

* Connect to Elasticsearch server:

        % curl http://localhost:9200/
        % curl http://localhost:9200/_cat/indices?v

        % curl http://localhost:5601/

* Connect to Dejavu server:

        % open 'http://localhost:1358/?appname=*&url=http://localhost:9200/&mode=edit'

* Stop it:

        % docker-compose down
