#!/bin/sh

id=8
image=amelon1/kibana

docker build \
    -t $image:$id \
    -t $image:latest \
    -f Dockerfile-kibana \
    .
